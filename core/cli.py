import argparse
import os
from pathlib import Path

import requests
import yaml
from tqdm import tqdm
from ppadb.client import Client as AdbClient

__PACKAGE_VERSION_URL = "https://f-droid.org/api/v1/packages/{app_name}"
__METADATA_URL = "https://gitlab.com/fdroid/fdroiddata/-/raw/master/metadata/{app_name}.yml"
__APK_URL = "https://f-droid.org/repo/{app_name}_{app_version}.apk"
__ARCHIVE_APK_URL = "https://f-droid.org/archive/{app_name}_{app_version}.apk"

__USER_HOME = f"{str(Path.home())}/.adbdroid"
__APP_CACHE = f"{__USER_HOME}/apps"


def parse_api_versions(_json) -> tuple[str, dict[int, str]]:
    available_versions: dict[int, str] = {}

    suggested_version_code = _json["suggestedVersionCode"]
    for pkg in _json["packages"]:
        version_name = pkg["versionName"]
        version_code = pkg["versionCode"]

        available_versions[version_code] = version_name

    return suggested_version_code, available_versions


def find_version(available_versions: dict[int, str], search_version: str) -> int | None:
    is_version_code = str.isdigit(search_version)
    if is_version_code and search_version in available_versions:
        return search_version

    for code, name in available_versions.items():
        if name == search_version:
            return code

    return None


def query_api(app_name: str, search_version=None) -> str:
    response = requests.get(__PACKAGE_VERSION_URL.format(app_name=app_name))
    if response.status_code != 200:
        raise Exception(response)

    suggested_code, available_versions = parse_api_versions(response.json())
    if search_version is None:
        return suggested_code, available_versions[suggested_code]

    result_version_code = find_version(available_versions, search_version)
    if result_version_code is not None:
        return result_version_code, available_versions[result_version_code]

    raise Exception(f"App does not have a version called '{search_version}'")


def download_app(app_name, version_code):
    print("Downloading app..")
    req = requests.get(__APK_URL.format(app_name=app_name, app_version=version_code), stream=True)
    if req.status_code == 404:
        print("Downloading app from archive..")
        req = requests.get(__ARCHIVE_APK_URL.format(app_name=app_name, app_version=version_code), stream=True)

    file_name = f"{__APP_CACHE}/{app_name}_{version_code}.apk"

    total = int(req.headers.get("Content-Length", 0))
    if not os.path.exists(file_name) or total > os.path.getsize(file_name):
        with open(file_name, "wb") as file, tqdm(desc=file_name, total=total, unit="iB", unit_scale=True,
                                                 unit_divisor=1024) as bar:
            for data in req.iter_content(chunk_size=1024):
                size = file.write(data)
                bar.update(size)
    else:
        print("App already exists, skipping download..")

    return file_name


def install_app(app, device=None):
    print("Installing app..")
    client = AdbClient(host="127.0.0.1", port=5037)
    devices = client.devices()

    if device is not None:
        client.device(device).install(app)
    else:
        for device in devices:
            device.install(app)

    print("Done!")


def parse_input():
    parser = argparse.ArgumentParser(description="ADB install applications from Fdroid")
    parser.add_argument("appname", help="The name of the app (e.g. org.mozilla.fennec_fdroid)")
    parser.add_argument("-v", "--version", help="Download a specific version of the app (version code or version name)")
    parser.add_argument("-d", "--device", help="ADB device identifier")

    args = parser.parse_args()

    if not os.path.exists(__APP_CACHE):
        os.makedirs(__APP_CACHE)

    version_code, version_name = query_api(args.appname, args.version)
    print(f"Installing {version_name} ({version_code})")
    if version_code is not None:
        path = download_app(args.appname, version_code)
        if path is not None:
            install_app(path, args.device)


if __name__ == "__main__":
    parse_input()
